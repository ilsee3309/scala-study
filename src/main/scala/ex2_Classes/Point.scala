package ex2_Classes

/**
  *
  * @author ilsee (<a href="mailto:ilsee@coupang.com">ilsee@coupang.com</a>)
  * @since 2017-01-30 23:30
  */
//var 제거하면 default로 val로 된다(상수).
class Point(var x: Int, var y: Int) {
//  Int // 생성자의 type Int는 class이고, 이 Int는 object이다. <- Singleton Obejects 챕터에서 부가 설명함.(object와 class가 왜 같이쓰이는지 - companions)

  def move(dx: Int, dy: Int): Unit = {
    x = x + dx
    y = y + dy
  }
  override def toString: String =
    "(" + x + ", " + y + ")"

}
