package ex1_UnifiedTypes

object UnifiedTypes extends App {
  val set = new scala.collection.mutable.LinkedHashSet[Any]
  set += "This is a string"
  set += 732
  set += 'c'
  set += true
  set += main _
  val iter: Iterator[Any] = set.iterator


  /**
    * type check
    */
//  List // scala.collection
//  Seq // scala.collection
//  Double //extends AnyVal
//  def sd = new AnyVal {} //extends Any
//  AnyRef // not extends Any

  while (iter.hasNext) {
    println(iter.next.toString())
  }
}