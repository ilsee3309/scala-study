package ex4_Mixin_Class_Composition

/**
  *
  * @author ilsee (<a href="mailto:ilsee@coupang.com">ilsee@coupang.com</a>)
  * @since 2017-01-31 03:00
  */
object StringIteratorTest {
  def main(args: Array[String]) {
    class Iter extends StringIterator(args(0)) with RichIterator
    val iter = new Iter
    iter.foreach(println)
    iter foreach println
  }
}