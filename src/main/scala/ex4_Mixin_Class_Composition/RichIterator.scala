package ex4_Mixin_Class_Composition

/**
  *
  * @author ilsee (<a href="mailto:ilsee@coupang.com">ilsee@coupang.com</a>)
  * @since 2017-01-31 03:00
  */
trait RichIterator extends AbsIterator {
  def foreach(f: T => Unit) { while (hasNext) f(next) }
}