package ex4_Mixin_Class_Composition

/**
  *
  * @author ilsee (<a href="mailto:ilsee@coupang.com">ilsee@coupang.com</a>)
  * @since 2017-01-31 02:58
  */
abstract class AbsIterator {
  type T
  def hasNext: Boolean
  def next: T
}