package ex4_Mixin_Class_Composition

/**
  *
  * @author ilsee (<a href="mailto:ilsee@coupang.com">ilsee@coupang.com</a>)
  * @since 2017-01-31 03:00
  */
class StringIterator(s: String) extends AbsIterator {
  type T = Char
  private var i = 0
  def hasNext = i < s.length()
  def next = { val ch = s charAt i; i += 1; ch }
}